## Container to redirect TCP connections to another target

Environment Vars:
| Name | Explanation | Example |
|---|---|---|
|PORT|Listen port for Socat| 8080 |
|TARGET_HOST| Hostname of target | google.be |
|TARGET_PORT| TCP port of target | 80 |